package com.omnirio.catalog.controller;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.omnirio.catalog.commons.CategoryDto;
import com.omnirio.catalog.commons.ProductAttributesDto;
import com.omnirio.catalog.commons.ProductDto;
import com.omnirio.catalog.entity.ProductAttributes;
import com.omnirio.catalog.services.CategryServiceImpl;
import com.sun.istack.NotNull;

import io.swagger.annotations.Api;
@Api
@RestController
@RequestMapping("/catalog")
public class CategoryController {

	@Autowired
	private CategryServiceImpl categoryController;

	@PostMapping(path = "/create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@Validated @RequestBody CategoryDto categoryDto) {
		return categoryController.createCategory(categoryDto);
	}

	@PostMapping(path = "/createAttributes", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createAttributes(@Validated @RequestBody ProductAttributesDto productAttributesDto) {
		return categoryController.createAttributes(productAttributesDto);
	}

	@PostMapping(path = "/createProduct", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createProduct(@Validated @RequestBody ProductDto productDto) {
		return categoryController.createProduct(productDto);
	}

	@GetMapping(path = "/atrributesByCategoryId/{categoryId}/{pageIndex}/{pageSize}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> atrributesByCategoryId(@PathVariable("categoryId") Long categoryId,
			@PathVariable("pageIndex") int pageIndex, @PathVariable("pageSize") int pageSize) {
		return categoryController.getCategoryById(categoryId, pageIndex, pageSize);
	}
	
	@GetMapping(path = "/productById/{productId}/{pageIndex}/{pageSize}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> productById(@PathVariable("productId") @NotNull String productId,
			@PathVariable("pageIndex") @Min(0) int pageIndex, @PathVariable("pageSize")@Min(0) int pageSize) {
		return categoryController.getProductById(productId, pageIndex, pageSize);
	}

}
