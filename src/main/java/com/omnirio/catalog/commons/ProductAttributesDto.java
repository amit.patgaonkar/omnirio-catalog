package com.omnirio.catalog.commons;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Component
public class ProductAttributesDto {

	private Long id;
	
	@NotNull(message = "Attribute  name can not be empty")
	@Size(min = 1,max = 100,message = "Attribute name should have minimum 1 and maximum 100 character")
	private String name;

	
	@NotNull(message = "Attribute  value can not be empty")
	@Size(min = 1,max = 100,message = "Attribute value should have minimum 1 and maximum 100 character")
	private String value;

	private boolean active;

}
