package com.omnirio.catalog.commons;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class ProductDto {

	private String id;

	@NotNull(message = "Product  name can not be empty")
	@Size(min = 1,max = 100,message = "Product  name should have minimum 1 and maximum 100 character")
	private String productName;

	@NotNull(message = "CategoryId  can not be empty")
	private Long categoryId;

	@NotNull(message = "Category name can not be empty")
	@Size(min = 1,max = 100,message = "Category name should have minimum 1 and maximum 100 character")
	private String categoryName;

	private boolean active;
	
	@Autowired
	private List<ProductAttributesDto> productAttributesDtos;
}
