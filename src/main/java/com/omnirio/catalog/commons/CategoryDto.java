package com.omnirio.catalog.commons;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CategoryDto {

	private Long id;

	@NotNull(message = "Category name can not be empty")
	@Size(min = 1, max = 100, message = "Category name should have minimum 1 and maximum 100 character")
	private String name;

	@Autowired
	private List<ProductAttributesDto> attributes;

	private boolean active;

}
