package com.omnirio.catalog.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.omnirio.catalog.commons.CategoryDto;
import com.omnirio.catalog.commons.ProductAttributesDto;
import com.omnirio.catalog.commons.ProductDto;
import com.omnirio.catalog.entity.Attributes;
import com.omnirio.catalog.entity.Category;
import com.omnirio.catalog.entity.CategoryAttributes;
import com.omnirio.catalog.entity.Product;
import com.omnirio.catalog.entity.ProductAttributes;
import com.omnirio.catalog.repository.CategoryRepository;
import com.omnirio.catalog.repository.ProductAttributesRepository;
import com.omnirio.catalog.repository.ProductRepository;
import com.omnirio.catalog.util.Utilities;

@Transactional
@Service
public class CategryServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(CategryServiceImpl.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductAttributesRepository productAttributesRepository;

	@Autowired
	private ProductRepository productRepository;

	private MultiValueMap<String, String> headers = null;

	private Map<String, String> param;

	private Category category;

	private Attributes productAttributes;

	private Product product;

	private List<Product> productsList;

	private List<ProductAttributes> productattributesList;

	private List<CategoryAttributes> categoryAttributesList;

	private List<Attributes> attributes;

	public ResponseEntity<?> createProduct(ProductDto productDto) {
		param = new HashMap<String, String>();
		headers = Utilities.getDefaultHeader();
		product = new Product();
		productattributesList = new ArrayList<ProductAttributes>();
		try {
			product.setId(productDto.getId());
			product.setActive(true);
			product.setProductName(productDto.getProductName());
			product.setCategoryId(productDto.getCategoryId());
			product.setCategoryName(productDto.getCategoryName());
			if (productDto.getProductAttributesDtos() != null) {
				for (ProductAttributesDto productAttributesDto : productDto.getProductAttributesDtos()) {
					ProductAttributes attributes = new ProductAttributes();
					attributes.setAttributeId(productAttributesDto.getId());
					attributes.setName(productAttributesDto.getName());
					attributes.setValue(productAttributesDto.getValue());

					// productAttributes.setAttributes(attributes);
					productattributesList.add(attributes);
				}
				product.setProductAttributes(new HashSet<>(productattributesList));
			}
			product = productRepository.save(product);

		} catch (Exception exception) {
			logger.error("CategryServiceImpl::createProduct::" + exception.getMessage());
		}
		if (product.getId() != null) {
			headers.add("STATUS", HttpStatus.OK.toString());
			headers.add("MESSAGE", productDto.getProductName() + "  created successfully");
			param.put("Status", HttpStatus.OK.toString());
			param.put("MESSAGE", productDto.getProductName() + "  created successfully");

		}
		return new ResponseEntity<>(param, headers, HttpStatus.OK);

	}

	public ResponseEntity<?> createCategory(CategoryDto categoryDto) {
		param = new HashMap<String, String>();
		headers = Utilities.getDefaultHeader();
		category = new Category();
		categoryAttributesList = new ArrayList<CategoryAttributes>();

		try {
			category.setId(categoryDto.getId());
			category.setActive(true);
			category.setName(categoryDto.getName());
			if (categoryDto.getAttributes() != null) {
				for (ProductAttributesDto productAttributesDto : categoryDto.getAttributes()) {
					CategoryAttributes categoryAttributes = new CategoryAttributes();
					Attributes attributes = new Attributes();
					attributes.setId(productAttributesDto.getId());
					categoryAttributes.setAttributes(attributes);
					categoryAttributesList.add(categoryAttributes);
				}
				category.setCategoryAttributes(new HashSet<>(categoryAttributesList));
			}
			category = categoryRepository.save(category);
		} catch (Exception exception) {
			logger.error("CategryServiceImpl::createCategory::" + exception.getMessage());
		}
		if (category.getId() != null) {
			headers.add("STATUS", HttpStatus.OK.toString());
			headers.add("MESSAGE", categoryDto.getName() + " category created successfully");
			param.put("Status", HttpStatus.OK.toString());
			param.put("MESSAGE", categoryDto.getName() + " category created successfully");

		}
		return new ResponseEntity<>(param, headers, HttpStatus.OK);

	}

	public ResponseEntity<?> createAttributes(ProductAttributesDto productAttributesDto) {
		param = new HashMap<String, String>();
		headers = Utilities.getDefaultHeader();
		productAttributes = new Attributes();
		try {
			productAttributes.setId(productAttributesDto.getId());
			productAttributes.setActive(true);
			productAttributes.setName(productAttributesDto.getName());
			productAttributes.setValue(productAttributesDto.getValue());
			productAttributes = productAttributesRepository.save(productAttributes);
		} catch (Exception exception) {
			logger.error("CategryServiceImpl::createAttributes::" + exception.getMessage());
		}
		if (productAttributes.getId() != null) {
			headers.add("STATUS", HttpStatus.OK.toString());
			headers.add("MESSAGE", productAttributesDto.getName() + " category attributes created successfully");
			param.put("Status", HttpStatus.OK.toString());
			param.put("MESSAGE", productAttributesDto.getName() + " category attributes created successfully");

		}
		return new ResponseEntity<>(param, headers, HttpStatus.OK);

	}

	public ResponseEntity<?> getCategoryById(Long categoryId, int pageIndex, int pageSize) {
		param = new HashMap<String, String>();
		headers = Utilities.getDefaultHeader();
		productattributesList = new ArrayList<ProductAttributes>();
		try {
			Pageable page = PageRequest.of(pageIndex, pageSize);
			attributes = categoryRepository.findAttributesByCategoryId(categoryId, page);
		} catch (Exception exception) {
			logger.error("CategryServiceImpl::createAttributes::" + exception.getMessage());
		}
		if (attributes != null && attributes.size() > 0) {
			headers.add("STATUS", HttpStatus.OK.toString());
			headers.add("MESSAGE", " category attributes for " + categoryId + " fetched successfully");
			// created successfully");
			param.put("Status", HttpStatus.OK.toString());
			// param.put("MESSAGE", productAttributesDto.getName() + " category attributes
			// created successfully");
			return new ResponseEntity<>(attributes, headers, HttpStatus.OK);

		}
		headers.add("STATUS", HttpStatus.NO_CONTENT.toString());
		headers.add("MESSAGE", " category attributes for " + categoryId + " not available");
		// created successfully");
		param.put("Status", HttpStatus.NO_CONTENT.toString());
		param.put("MESSAGE", " category attributes for " + categoryId + " not available");
		// created successfully");
		return new ResponseEntity<>(param, headers, HttpStatus.OK);

	}

	public ResponseEntity<?> getProductById(String productId, int pageIndex, int pageSize) {
		param = new HashMap<String, String>();
		headers = Utilities.getDefaultHeader();
		productsList = new ArrayList<Product>();
		try {
			Pageable page = PageRequest.of(pageIndex, pageSize);
			Page<Product> pageProduct = productRepository.findById(productId, page);
			productsList = pageProduct.getContent();
		} catch (Exception exception) {
			logger.error("CategryServiceImpl::getProductById::" + exception.getMessage());
		}
		if (productsList != null && productsList.size() > 0) {
			headers.add("STATUS", HttpStatus.OK.toString());
			headers.add("MESSAGE", " Product for " + productId + " fetched successfully");
			// created successfully");
			param.put("Status", HttpStatus.OK.toString());
			// param.put("MESSAGE", productAttributesDto.getName() + " category attributes
			// created successfully");
			return new ResponseEntity<>(productsList, headers, HttpStatus.OK);

		}
		headers.add("STATUS", HttpStatus.NO_CONTENT.toString());
		headers.add("MESSAGE", " Product for " + productId + " not available");
		// created successfully");
		param.put("Status", HttpStatus.NO_CONTENT.toString());
		param.put("MESSAGE", " Product for " + productId + " not available");
		// created successfully");
		return new ResponseEntity<>(param, headers, HttpStatus.OK);

	}

}
