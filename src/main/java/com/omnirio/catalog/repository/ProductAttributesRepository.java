package com.omnirio.catalog.repository;

import org.springframework.data.repository.CrudRepository;

import com.omnirio.catalog.entity.Attributes;

public interface ProductAttributesRepository extends CrudRepository<Attributes, Long>{

}
