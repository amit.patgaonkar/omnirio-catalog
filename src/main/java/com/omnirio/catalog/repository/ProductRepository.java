package com.omnirio.catalog.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.omnirio.catalog.entity.Product;

public interface ProductRepository extends CrudRepository<Product, String>{

	Page<Product> findById(String id,Pageable pageable);
}
