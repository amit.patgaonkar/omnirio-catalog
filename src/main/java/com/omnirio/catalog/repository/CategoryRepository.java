package com.omnirio.catalog.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.omnirio.catalog.entity.Attributes;
import com.omnirio.catalog.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
	
	@Query(value = "SELECT attributes FROM CategoryAttributes e where e.catagoryId=:catagoryId")
	public List<Attributes> findAttributesByCategoryId(@Param("catagoryId") Long catagoryId,Pageable page);
}
