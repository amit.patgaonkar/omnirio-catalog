package com.omnirio.catalog.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "category")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@SequenceGenerator(name = "")
	@Column(name = "id", nullable = false, length = 100)
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	private String name;

	// bi-directional many-to-one association to CaseSheetDetails
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "catagoryId", referencedColumnName = "id")
	private Set<CategoryAttributes> categoryAttributes;

	@Column(name = "active", nullable = false, columnDefinition = "tinyint(1) default 1")
	private boolean active;

}
