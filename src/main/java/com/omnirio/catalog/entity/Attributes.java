package com.omnirio.catalog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "attributes")
@Setter
@Getter
public class Attributes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@SequenceGenerator(name="")
	@Column(name = "id", nullable = false, length = 100)
	private Long id;
	
	@Column(name = "name", nullable = false, length = 100)
	private String name;
	
	@Column(name = "value", nullable = false, length = 200)
	private String value;
	
	@Column(name = "active", nullable = false, columnDefinition = "tinyint(1) default 1")
	private boolean active;

	
	
}
