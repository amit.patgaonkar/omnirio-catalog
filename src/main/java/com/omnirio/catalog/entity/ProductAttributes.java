package com.omnirio.catalog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "productAttributes")
@Setter
@Getter
public class ProductAttributes {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@SequenceGenerator(name = "")
	@Column(name = "id", nullable = false, length = 100)
	private Long id;

	@Column
	private String name;

	@Column
	private String value;
	
	@Column
	private Long attributeId;
}
