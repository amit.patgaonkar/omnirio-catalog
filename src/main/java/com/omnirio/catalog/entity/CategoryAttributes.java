package com.omnirio.catalog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "categoryattributes")
public class CategoryAttributes {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@SequenceGenerator(name = "")
	@Column(name = "id", nullable = false, length = 100)
	private Long id;

	// single-directional many-to-one association to SysWidgets
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "attributeIdS")
	private Attributes attributes;

	@Column
	private Long catagoryId;

}
