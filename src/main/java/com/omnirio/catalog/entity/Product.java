package com.omnirio.catalog.entity;

import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product")
@Setter
@Getter
public class Product {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "id", nullable = false, length = 100)
	private String id;

	@Column(name = "productName", nullable = false, length = 100)
	private String productName;

	@Column(name = "categoryId", nullable = false)
	private Long categoryId;

	@Column(name = "categoryName", nullable = false, length = 100)
	private String categoryName;

	// bi-directional many-to-one association to CaseSheetDetails
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", referencedColumnName = "id")
	private Set<ProductAttributes> productAttributes;
	
	@Column(name = "active", nullable = false, columnDefinition = "tinyint(1) default 1")
	private boolean active;
}
