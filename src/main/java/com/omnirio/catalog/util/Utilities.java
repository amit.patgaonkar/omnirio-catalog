package com.omnirio.catalog.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

public class Utilities {

	private static Logger logger = LoggerFactory.getLogger(Utilities.class);

		public static MultiValueMap<String, String> getDefaultHeader() {
		MultiValueMap<String, String> headers = new HttpHeaders();
		StringBuilder exposeHeaders = new StringBuilder();
		exposeHeaders.append("STATUS");
		exposeHeaders.append(",");
		exposeHeaders.append("MESSAGE");
		exposeHeaders.append(",");
		headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, exposeHeaders.toString());
		return headers;
	}

}
